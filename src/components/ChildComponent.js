import React, { useState } from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap'
const ChildComponent = props => {
  const [id, setId] = useState(Object.keys(props.sendElementEdit).length === 0 ? "" : props.sendElementEdit.id);
  const [nombre, setNombre] = useState(Object.keys(props.sendElementEdit).length === 0 ? "" : props.sendElementEdit.nombre);
  const [apellido, setApellido] = useState(Object.keys(props.sendElementEdit).length === 0 ? "" : props.sendElementEdit.apellido);
  const sendData = () => {
    props.handleParentData(id, nombre, apellido);
  }
  return (
    <Form>
      <Form.Group as={Row}>
        <Form.Label column sm="2">
          Numero:
        </Form.Label>
        <Col sm="7">
          <Form.Control
            onChange={e => setId(Number(e.target.value))}
            placeholder={Object.keys(props.sendElementEdit).length === 0 ? "Ingrese Numero de Tabla" : props.sendElementEdit.id}
            disabled={Object.keys(props.sendElementEdit).length === 0 ? false : true} />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="2">
          Nombres:
        </Form.Label>
        <Col sm="7">
          <Form.Control
            onChange={e => setNombre(e.target.value)}
            placeholder={Object.keys(props.sendElementEdit).length === 0 ? "Ingrese Nombre de la Tabla" : props.sendElementEdit.nombre} />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="2">
          Apellidos:
        </Form.Label>
        <Col sm="7">
          <Form.Control
            onChange={e => setApellido(e.target.value)}
            placeholder={Object.keys(props.sendElementEdit).length === 0 ? "Ingrese Apellido de Tabla" : props.sendElementEdit.apellido} />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Col sm="6">
          <Button variant="secondary" onClick={props.handleClose}>
            Cerrar
          </Button>
        </Col>
        <Col sm="6">
          <Button variant="primary" onClick={sendData}>
            Guardar Datos
          </Button>
        </Col>
      </Form.Group>
    </Form>
  );
}
export default ChildComponent;