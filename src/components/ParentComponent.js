import React, { useState } from 'react'
import { Button, Col, Container, Modal, Row, Table } from 'react-bootstrap';
import ChildComponent from './ChildComponent';
const ParentComponent = () => {
  const [show, setShow] = useState(false);
  const [element, setElement] = useState({});
  const [table, setTable] = useState([{
    id: 1,
    nombre: "Bryan",
    apellido: "Alvarado"
  }, {
    id: 2,
    nombre: "Alex",
    apellido: "Lauri"
  }]);
  const handleParentData = (id, nombre, apellido) => {
    let search = table.filter(t => t.id === id);
    if (search.length === 0) {
      setTable([...table, { id, nombre, apellido }]);
      handleClose();
    }
    else {
      let index = table.findIndex((e => e.id === id));
      table[index].nombre = nombre;
      table[index].apellido = apellido;
      setTable([...table])
      handleClose();
    }
  }
  const handleClose = () => {
    setShow(false)
  };
  const handleShow = () => {
    setShow(true);
  }
  const handleCreateRow = () => {
    setElement({});
    handleShow();
  }
  let deleteElementFromTable = id => {
    setTable(table.filter(e => e.id !== id));
  }
  let editElementFromTable = t => {
    setElement(t);
    handleShow();
  }
  return (
    <Container>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Agrege Datos a la Tabla</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ChildComponent handleClose={handleClose} sendElementEdit={element} handleParentData={handleParentData} />
        </Modal.Body>
      </Modal>
      <Row>
        <Col>
          Tabla que vamos Agregar Datos
        </Col>
      </Row>
      <Row >
        <Col>
          <Button onClick={handleCreateRow} variant="outline-primary">Agregar Valores a la Tabla</Button>
        </Col>
      </Row>
      {
        table.length > 0 ?
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Eliminar</th>
                <th>Editar</th>
              </tr>
            </thead>
            <tbody>
              {table
                .map((t, index) =>
                  <tr key={index}>
                    <td>{t.id}</td>
                    <td>{t.nombre}</td>
                    <td>{t.apellido}</td>
                    <td>
                      <Button onClick={() => deleteElementFromTable(t.id)} variant="danger" size="sm">
                        Eliminar
                      </Button>
                    </td>
                    <td>
                      <Button onClick={() => editElementFromTable(t)} variant="secondary" size="sm">
                        Editar
                      </Button>
                    </td>
                  </tr>)
              }
            </tbody>
          </Table> : "No hay Datos Que Mostrar Por Favor Agrega Datos"
      }
    </Container>
  );
}
export default ParentComponent;
